// import React, { useEffect, useState } from "react";
// import {
//   BrowserRouter as Router,
//   Route as DefaultRoute,
//   Routes,
// } from 'react-router-dom';
// // import AppLayout from "./shared/layout/app-layout";
// // import DefaultLayout from "./shared/layout/default-layout";
// import { Navigate } from "react-router";
// import DefaultLayout from "src/shared/layout/default-layout";
// import Login from "./modules/login";

// const AppRoutes = (props: any) => {
//   const { isAuthenticated = false } = props;

//   const Route = ({
//     component: Component,
//     layout: Layout = DefaultLayout,
//     ...rest
//   }: any) => {
//     return (
//       // <DefaultRoute
//       //   {...rest}
//       //   element={(renderProps: any) => {
//       //     return (
//       //       <Layout
//       //         {...renderProps}
//       //         {...props}
//       //       >
//       //         <Component {...renderProps} />
//       //       </Layout>
//       //     )
//       //   }
//       //   }
//       // />
//       <DefaultRoute  {...rest} element={<Login />} />
//     )
//   }

//   return (
//     // <Router>
//     //   <Routes>
//     //     <DefaultRoute
//     //       // exact
//     //       path="/"
//     //       element={() => {
//     //         return (
//     //           isAuthenticated ?
//     //             <Navigate to="/services"/> :
//     //             <Navigate to="/login" />
//     //         )
//     //       }}
//     //     />

//     //     <Route exact path="/login" component={Login} />

//     //     <Route exact path="/"
//     //       component={Home}
//     //       layout={AppLayout} />

//     //     <Route path='/404'
//     //       {...(
//     //         isAuthenticated && {
//     //           layout: AppLayout
//     //         }
//     //       )}
//     //       component={PageNotFound} />

//     //     { isAuthenticated ?
//     //     (
//     //     <Navigate to='/404' />
//     //     ) :
//     //     <Navigate to='/' />
//     //     }
//     //   </Routes>
//     // </Router>
//     <Router>
//       <Routes>
//         {/* <DefaultRoute path="/login" element={<Login />} /> */}
//         <Route path="/login" component={Login} />
//         {/* {Route({ component: Login, path: "/login" })} */}
//       </Routes>
//     </Router>
//   );
// };

// export default AppRoutes;

import React, { useEffect, useState } from "react";
import Login from "./modules/login";
import {
  BrowserRouter as Router,
  Route as DefaultRoute,
  Routes,
  Navigate,
  Outlet,
  useNavigate,
} from "react-router-dom";
import AppLayout from "./shared/layout/app-layout";
import { DefaultLayout } from "./shared/layout/default-layout";

const AppRoutes = (props: any) => {
  const { userToken } = props;

  return (
    <Router>
      <Routes>
        {!userToken ? (
          <DefaultRoute path="/" element={<DefaultLayout />}>
            <DefaultRoute path="/login" element={<Login />} />
            <DefaultRoute path="*" element={<Navigate replace to="/login" />} />
          </DefaultRoute>
        ) : (
          <DefaultRoute path="/" element={<AppLayout />}>
            <DefaultRoute path="/workSpace/:workSpaceId/channel/:channelId" element={<Login />} />
            <DefaultRoute path="/workSpace/:workSpaceId/user/:userId" element={<Login />} />
            <DefaultRoute path="/client" element={<Login />} />
            <DefaultRoute path="*" element={<Navigate replace to="/client" />} />
          </DefaultRoute>
        )}
        <DefaultRoute
          path="*"
          element={<Navigate replace to={!userToken ? "/login" : "/client"} />}
        />
      </Routes>
    </Router>
  );
};

export default AppRoutes;
