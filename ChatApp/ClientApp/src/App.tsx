import React, { Fragment, useEffect, useState } from "react";
import "./App.scss";
import { connect } from "react-redux";
import AppRoutes from "src/routes";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faB,
  faCheckSquare,
  faCoffee,
  faSearch,
  faChevronDown,
  faCaretRight,
  faCaretDown,
  faPlus,
  faHashtag,
  faClose,
  faLock,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faB,
  faCheckSquare,
  faCoffee,
  faSearch,
  faChevronDown,
  faCaretRight,
  faCaretDown,
  faPlus,
  faHashtag,
  faClose,
  faLock
);

export interface IAppProps extends StateProps, DispatchProps {}

export const App = (props: IAppProps) => {
  return <AppRoutes {...props} />;
};
const mapStateToProps = ({ loginReducer }: any) => ({
  userToken: loginReducer?.userToken,
});

const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(App);
