import { call, put, select } from "redux-saga/effects";
import createReducer from "src/utils/reducer/createReducer";
import { getUserToken } from "src/utils/reducer/getUserToken";
import api from "src/utils/constants/api";

// Reducer
export const FETCH_ALL_CHANNELS = "FETCH_ALL_CHANNELS";
export const FETCH_ALL_CHANNELS_SUCCESS = "FETCH_ALL_CHANNELS_SUCCESS";
export const FETCH_ALL_CHANNELS_FAILURE = "FETCH_ALL_CHANNELS_FAILURE";

export function getAllChannels(payload = {}) {
  return {
    type: FETCH_ALL_CHANNELS,
    payload,
  };
}

export function getAllChannelsSuccess(payload) {
  return {
    type: FETCH_ALL_CHANNELS_SUCCESS,
    payload,
  };
}

export function getAllChannelsFailure(payload) {
  return {
    type: FETCH_ALL_CHANNELS_FAILURE,
    payload,
  };
}

const initialState = {
  fetchAllChannelsResponse: null,
};

export const channelsReducer = createReducer(initialState, {
  [FETCH_ALL_CHANNELS](state) {
    return {
      ...state,
    };
  },
  [FETCH_ALL_CHANNELS_SUCCESS](state, action) {
    return {
      ...state,
      fetchAllChannelsResponse: action?.payload,
    };
  },
  [FETCH_ALL_CHANNELS_FAILURE](state, action) {
    return {
      ...state,
      fetchAllChannelsResponse: action?.payload,
    };
  },
});

// API
const getAllChannelsApi = (payload, userToken) => {
  const pathUrl = `WorkSpaceChannels/workSpace/${payload?.workSpaceId}/channels`;
  return api(pathUrl, null, "get", userToken);
};

// Saga
export function* getAllChannelsSaga({ payload }) {
  const userToken = yield select(getUserToken);
  const fetchAllChannelsResponse = yield call(
    getAllChannelsApi,
    payload,
    userToken
  );
  try {
    if (fetchAllChannelsResponse) {
      const successMessage = {
        success: true,
        data: fetchAllChannelsResponse,
      };
      yield put(getAllChannelsSuccess(successMessage));
    } else {
      const failedResponse = {
        success: false,
        errorResponse: fetchAllChannelsResponse,
      };
      yield put(getAllChannelsFailure(failedResponse));
    }
  } catch (ex) {
    const failedResponse = {
      success: false,
      error: "Failed",
    };
    yield put(getAllChannelsFailure(failedResponse));
  }
}
