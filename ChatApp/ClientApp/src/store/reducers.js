import { loginReducer } from "./useLoginRequestReducer";
import { workSpaceReducer } from "./useAllWorkSpacesReducer";
import { channelsReducer } from "./useAllChannelsReducer";
import { usersReducer } from "./useAllUsersReducer";

export default {
  workSpaceReducer,
  loginReducer,
  channelsReducer,
  usersReducer,
};
