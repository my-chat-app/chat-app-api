/**
 *  Redux saga class init
 */
import { takeEvery, all } from "redux-saga/effects";
import { REQUEST_LOGIN, loginSaga } from "./useLoginRequestReducer";
import {
  FETCH_ALL_WORK_SPACES,
  getAllWorkSpacesSaga,
} from "./useAllWorkSpacesReducer";
import {
  FETCH_ALL_CHANNELS,
  getAllChannelsSaga,
} from "./useAllChannelsReducer";
import { FETCH_ALL_USERS, getAllUsersSaga } from "./useAllUsersReducer";

export default function* watch() {
  yield all([takeEvery(REQUEST_LOGIN, loginSaga)]);
  yield all([takeEvery(FETCH_ALL_WORK_SPACES, getAllWorkSpacesSaga)]);
  yield all([takeEvery(FETCH_ALL_CHANNELS, getAllChannelsSaga)]);
  yield all([takeEvery(FETCH_ALL_USERS, getAllUsersSaga)]);
}
