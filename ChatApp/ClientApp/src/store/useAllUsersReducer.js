import { call, put, select } from "redux-saga/effects";
import createReducer from "src/utils/reducer/createReducer";
import { getUserToken } from "src/utils/reducer/getUserToken";
import api from "src/utils/constants/api";

// Reducer
export const FETCH_ALL_USERS = "FETCH_ALL_USERS";
export const FETCH_ALL_USERS_SUCCESS = "FETCH_ALL_USERS_SUCCESS";
export const FETCH_ALL_USERS_FAILURE = "FETCH_ALL_USERS_FAILURE";

export function getAllUsers(payload = {}) {
  return {
    type: FETCH_ALL_USERS,
    payload,
  };
}

export function getAllUsersSuccess(payload) {
  return {
    type: FETCH_ALL_USERS_SUCCESS,
    payload,
  };
}

export function getAllUsersFailure(payload) {
  return {
    type: FETCH_ALL_USERS_FAILURE,
    payload,
  };
}

const initialState = {
  fetchAllUsersResponse: null,
};

export const usersReducer = createReducer(initialState, {
  [FETCH_ALL_USERS](state) {
    return {
      ...state,
    };
  },
  [FETCH_ALL_USERS_SUCCESS](state, action) {
    return {
      ...state,
      fetchAllUsersResponse: action?.payload,
    };
  },
  [FETCH_ALL_USERS_FAILURE](state, action) {
    return {
      ...state,
      fetchAllUsersResponse: action?.payload,
    };
  },
});

// API
const getAllUsersApi = (payload, userToken) => {
  const pathUrl = `WorkSpaceUsers/workSpace/${payload?.workSpaceId}/users`;
  return api(pathUrl, null, "get", userToken);
};

// Saga
export function* getAllUsersSaga({ payload }) {
  const userToken = yield select(getUserToken);
  const fetchAllUsersResponse = yield call(
    getAllUsersApi,
    payload,
    userToken
  );
  try {
    if (fetchAllUsersResponse) {
      const successMessage = {
        success: true,
        data: fetchAllUsersResponse,
      };
      yield put(getAllUsersSuccess(successMessage));
    } else {
      const failedResponse = {
        success: false,
        errorResponse: fetchAllUsersResponse,
      };
      yield put(getAllUsersFailure(failedResponse));
    }
  } catch (ex) {
    const failedResponse = {
      success: false,
      error: "Failed",
    };
    yield put(getAllUsersFailure(failedResponse));
  }
}
