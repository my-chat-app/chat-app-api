import { call, put, select } from "redux-saga/effects";
import createReducer from "src/utils/reducer/createReducer";
import { getUserToken } from "src/utils/reducer/getUserToken";
import api from "src/utils/constants/api";

// Reducer
export const FETCH_ALL_WORK_SPACES = "FETCH_ALL_WORK_SPACES";
export const FETCH_ALL_WORK_SPACES_SUCCESS = "FETCH_ALL_WORK_SPACES_SUCCESS";
export const FETCH_ALL_WORK_SPACES_FAILURE = "FETCH_ALL_WORK_SPACES_FAILURE";

export const UPDATE_CURRENT_WORK_SPACE = "UPDATE_CURRENT_WORK_SPACE";

export function getAllWorkSpaces(payload = {}) {
  return {
    type: FETCH_ALL_WORK_SPACES,
    payload,
  };
}

export function getAllWorkSpacesSuccess(payload) {
  return {
    type: FETCH_ALL_WORK_SPACES_SUCCESS,
    payload,
  };
}

export function getAllWorkSpacesFailure(payload) {
  return {
    type: FETCH_ALL_WORK_SPACES_FAILURE,
    payload,
  };
}

export function updateCurrentWorkSpace(payload) {
  return {
    type: UPDATE_CURRENT_WORK_SPACE,
    payload,
  };
}

const initialState = {
  fetchAllWorkSpacesResponse: null,
  currentWorkSpace: null,
};

export const workSpaceReducer = createReducer(initialState, {
  [FETCH_ALL_WORK_SPACES](state) {
    return {
      ...state,
    };
  },
  [FETCH_ALL_WORK_SPACES_SUCCESS](state, action) {
    return {
      ...state,
      fetchAllWorkSpacesResponse: action?.payload,
    };
  },
  [FETCH_ALL_WORK_SPACES_FAILURE](state, action) {
    return {
      ...state,
      fetchAllWorkSpacesResponse: action?.payload,
    };
  },
  [UPDATE_CURRENT_WORK_SPACE](state, action) {
    return {
      ...state,
      currentWorkSpace: action?.payload,
    };
  },
});

// API
const getAllWorkSpacesApi = (payload, userToken) => {
  const pathUrl = `WorkSpace`;
  return api(pathUrl, null, "get", userToken);
};

// Saga
export function* getAllWorkSpacesSaga({ payload }) {
  const userToken = yield select(getUserToken);
  const fetchAllWorkSpacesResponse = yield call(
    getAllWorkSpacesApi,
    payload,
    userToken
  );
  try {
    if (fetchAllWorkSpacesResponse) {
      const successMessage = {
        success: true,
        data: fetchAllWorkSpacesResponse,
      };
      yield put(getAllWorkSpacesSuccess(successMessage));
    } else {
      const failedResponse = {
        success: false,
        errorResponse: fetchAllWorkSpacesResponse,
      };
      yield put(getAllWorkSpacesFailure(failedResponse));
    }
  } catch (ex) {
    const failedResponse = {
      success: false,
      error: "Failed",
    };
    yield put(getAllWorkSpacesFailure(failedResponse));
  }
}
