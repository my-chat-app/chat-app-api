import { call, put } from "redux-saga/effects";
import api from "src/utils/constants/api";
import createReducer from "../utils/reducer/createReducer";

export const REQUEST_LOGIN = "REQUEST_LOGIN";
export const REQUEST_LOGIN_SUCCESS = "REQUEST_LOGIN_SUCCESS";
export const REQUEST_LOGIN_FAILURE = "REQUEST_LOGIN_FAILURE";

export function login(payload) {
  return {
    type: REQUEST_LOGIN,
    payload,
  };
}

export function loginSuccess(payload) {
  return {
    type: REQUEST_LOGIN_SUCCESS,
    payload,
  };
}

export function loginFailure(payload) {
  return {
    type: REQUEST_LOGIN_FAILURE,
    payload,
  };
}

const initialState = {
  loginResponse: null,
  userToken: null,
};

export const loginReducer = createReducer(initialState, {
  [REQUEST_LOGIN](state) {
    return {
      ...state,
    };
  },
  [REQUEST_LOGIN_SUCCESS](state, action) {
    return {
      ...state,
      loginResponse: action?.payload,
      userToken: action?.payload?.data?.token,
    };
  },
  [REQUEST_LOGIN_FAILURE](state, action) {
    return {
      ...state,
      loginResponse: action?.payload,
    };
  },
});

// API
const loginApi = (payload) => {
  const pathUrl = `users/authenticate`;
  return api(pathUrl, payload, "post", null);
};

// Saga
export function* loginSaga({ payload }) {
  const loginResponse = yield call(loginApi, payload);
  try {
    if ("token" in loginResponse) {
      const successMessage = {
        success: true,
        data: loginResponse,
      };
      yield put(loginSuccess(successMessage));
    } else {
      const failedResponse = {
        success: false,
        errorResponse: loginResponse,
      };
      yield put(loginFailure(failedResponse));
    }
  } catch (ex) {
    const failedResponse = {
      success: false,
      error: "Login Failed",
    };
    yield put(loginFailure(failedResponse));
  }
}
