// General api to access data
import ApiConstants from './ApiConstants';
export default function api(path, params, method, token, jsonRes = true) {
  let options;
  options = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(token && { Authorization: `Bearer ${token}` }),
    },
    method: method,
    ...(params && { body: JSON.stringify(params) }),
  };
  return fetch(ApiConstants.BASE_URL + path, options)
    .then(resp =>
      resp.status === ApiConstants.CREATED_STATUS_CODE
        ? resp
        : jsonRes
          ? resp.json()
          : resp,
    )
    .then(json => json)
    .catch(error => error);
}
