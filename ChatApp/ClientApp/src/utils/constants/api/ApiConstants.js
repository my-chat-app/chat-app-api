/* App config for apis
 */
const ApiConstants = {
  BASE_URL: 'https://localhost:5001/',
  CREATED_STATUS_CODE: 201,
  SUCCESS_STATUS_CODE: 200,
  FAILURE_STATUS_CODE: 400,
};

export default ApiConstants;
