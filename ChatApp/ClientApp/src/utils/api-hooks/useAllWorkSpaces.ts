import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { call, put, select } from "redux-saga/effects";
import {
  getAllWorkSpaces,
  updateCurrentWorkSpace,
} from "src/store/useAllWorkSpacesReducer";

export const useAllWorkSpaces = () => {
  const dispatch = useDispatch();
  const fetchAllWorkSpacesResponse = useSelector(
    (state: any) => state?.workSpaceReducer?.fetchAllWorkSpacesResponse
  );
  const [loading, setLoading] = useState(false);
  const [workSpaces, setWorkSpaces] = useState([]);

  useEffect(() => {
    setLoading(true);
    dispatch(getAllWorkSpaces());
  }, []);

  useEffect(() => {
    if (loading) {
      if (fetchAllWorkSpacesResponse?.success) {
        const { data } = fetchAllWorkSpacesResponse;
        setWorkSpaces(data?.workSpaces);
        dispatch(updateCurrentWorkSpace(data?.workSpaces[0] ?? null));
      }
      setLoading(false);
    }
  }, [fetchAllWorkSpacesResponse]);

  return { data: workSpaces, loading };
};

export default useAllWorkSpaces;
