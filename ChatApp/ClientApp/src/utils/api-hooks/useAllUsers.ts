import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllUsers } from "src/store/useAllUsersReducer";
import useCurrentWorkSpace from "../hooks/useCurrentWorkSpace";

export const useAllUsers = () => {
  const dispatch = useDispatch();
  const fetchAllUsersResponse = useSelector(
    (state: any) => state?.usersReducer?.fetchAllUsersResponse
  );
  const currentWorkSpace = useCurrentWorkSpace();
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    setLoading(true);
    dispatch(getAllUsers({ workSpaceId: currentWorkSpace?.id }));
  }, [currentWorkSpace]);

  useEffect(() => {
    if (loading) {
      if (fetchAllUsersResponse?.success) {
        const { data } = fetchAllUsersResponse;
        setUsers(data);
      }
      setLoading(false);
    }
  }, [fetchAllUsersResponse]);

  return { data: users, loading };
};

export default useAllUsers;
