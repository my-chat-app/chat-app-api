import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { call, put, select } from "redux-saga/effects";
import { getAllChannels } from "src/store/useAllChannelsReducer";
import useCurrentWorkSpace from "../hooks/useCurrentWorkSpace";

export const useAllChannels = () => {
  const dispatch = useDispatch();
  const fetchAllChannelsResponse = useSelector(
    (state: any) => state?.channelsReducer?.fetchAllChannelsResponse
  );
  const currentWorkSpace = useCurrentWorkSpace();
  const [loading, setLoading] = useState(false);
  const [channels, setChannels] = useState([]);

  useEffect(() => {
    setLoading(true);
    dispatch(getAllChannels({ workSpaceId: currentWorkSpace?.id }));
  }, [currentWorkSpace]);

  useEffect(() => {
    if (loading) {
      if (fetchAllChannelsResponse?.success) {
        const { data } = fetchAllChannelsResponse;
        setChannels(data);
      }
      setLoading(false);
    }
  }, [fetchAllChannelsResponse]);

  return { data: channels, loading };
};

export default useAllChannels;
