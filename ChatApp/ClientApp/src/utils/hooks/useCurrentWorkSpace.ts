import { useSelector } from "react-redux";

export const useCurrentWorkSpace = () => {
  const currentWorkSpace = useSelector(
    (state: any) => state?.workSpaceReducer?.currentWorkSpace
  );
  return currentWorkSpace;
};

export default useCurrentWorkSpace;
