import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Button,
  Col,
  Container,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import "./style.scss";
import { login } from "src/store/useLoginRequestReducer";
import AppSpinner from "src/shared/components/app-spinner";

export interface IAppProps extends StateProps, DispatchProps {}

export const Login = (props: IAppProps) => {
  const { loginResponse } = props;

  const [emailError, setEmailError] = useState(null);
  const [passwordError, setPasswordError] = useState(null);
  const [loading, setLoading] = useState(false);
  useEffect(() => {}, []);

  const onLogin = (event: any) => {
    event.preventDefault();
    setEmailError(null);
    setPasswordError(null);
    setLoading(true);
    props.login({
      username: event.target[0].value,
      password: event.target[1].value,
    });
  };

  useEffect(() => {
    if (loading) {
      if (loginResponse?.success) {
        setLoading(false);
      } else {
        setLoading(false);
        setEmailError(loginResponse?.errorResponse.errors?.Username);
        setPasswordError(
          loginResponse?.errorResponse.errors?.Password ??
            loginResponse?.errorResponse?.message
        );
      }
    }
  }, [loginResponse]);

  return (
    <Container>
      <Row className="pb-3 pt-3">
        <Col lg={4} />
        <Col lg={4}>
          <div className="align-items-center justify-content-center text-center mb-3 mt-3">
            <h4>Logo</h4>
          </div>
          <div className="align-items-center justify-content-center text-center mb-3 mt-3">
            <h3>Sign In to WORK SPACE</h3>
          </div>

          <Col className="align-items-center d-flex justify-content-center mb-3 mt-3">
            <Button className="btn btn-lg btn-google btn-block text-uppercase btn-outline google-button">
              <img src="https://img.icons8.com/color/16/000000/google-logo.png"></img>{" "}
              Sign In Using Google
            </Button>
          </Col>

          <div className="separator mb-3 mt-3">OR</div>

          <Col>
            <Form onSubmit={onLogin}>
              <FormGroup>
                <Label for="exampleEmail">Email Address</Label>
                <Input
                  type="email"
                  name="email"
                  id="exampleEmail"
                  placeholder="Your Email"
                  invalid={emailError !== null}
                />
                <FormFeedback>{emailError}</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="examplePassword">Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="examplePassword"
                  placeholder="Your Password"
                  invalid={passwordError !== null}
                />
                <FormFeedback>{passwordError}</FormFeedback>
              </FormGroup>
              <Button className="w-100 mb-3 mt-3">
                <b className="m-0">Sign In</b>
              </Button>
            </Form>
          </Col>
        </Col>
        <Col lg={4} />
      </Row>
      <AppSpinner loading={loading} />
    </Container>
  );
};
const mapStateToProps = ({ loginReducer }: any) => ({
  loginResponse: loginReducer?.loginResponse,
  userToken: loginReducer?.userToken,
});

const mapDispatchToProps = { login };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Login);
