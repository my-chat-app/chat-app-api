import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Image } from "react-bootstrap";
import { connect } from "react-redux";
import { Outlet, useNavigate } from "react-router";
import {
  Button,
  Col,
  Container,
  FormGroup,
  Input,
  InputGroup,
  Label,
  Navbar,
  Row,
} from "reactstrap";
import "./styles.scss";

export const Header = () => {
  const navigate = useNavigate();

  useEffect(() => {
    //@ts-ignore
    if (window.location.pathname === "/") {
      navigate("/home");
    }
    //@ts-ignore
  }, [window.location.pathname]);

  return (
    <Navbar >
      <Col xs={12} sm={12} lg={12} xxl={12}>
        <Row>
          <Col xs={2} sm={2} md={4} xxl={4} className=""></Col>
          <Col
            xs={8}
            sm={8}
            md={4}
            xxl={4}
            className="p-0 d-flex justify-content-center align-items-center"
          >
            <Button className="w-100 search-button">
              <Col xs={12} sm={12} md={12} xxl={12}>
                <Row>
                  <Col
                    xs={6}
                    sm={6}
                    md={6}
                    xxl={6}
                    className="d-flex justify-content-start align-items-center"
                  >
                    <span>Search</span>
                  </Col>
                  <Col
                    xs={6}
                    sm={6}
                    md={6}
                    xxl={6}
                    className="d-flex justify-content-end align-items-center"
                  >
                    <FontAwesomeIcon icon="search" />
                  </Col>
                </Row>
              </Col>
            </Button>
          </Col>
          <Col
            xs={2}
            sm={2}
            md={4}
            xxl={4}
            className="d-flex justify-content-end align-items-center"
          >
            <Button className="m-0 p-0 position-relative">
              <Image
                rounded
                src={
                  "https://ca.slack-edge.com/T032L5QT283-U0339TCD6QY-d5ae86c58889-32"
                }
              />
              <div className="online-status-box online"></div>
            </Button>
          </Col>
        </Row>
      </Col>
    </Navbar>
  );
};

const mapStateToProps = ({}: any) => ({});

const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Header);
