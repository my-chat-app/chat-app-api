import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import "./styles.scss";

export const AccordionChannelItem = ({
  id,
  title,
  isPrivate,
  onClick,
  selected = false,
  type,
}) => {
  return (
    <div
      onClick={() => onClick(id, type)}
      className={`d-flex flex-row align-items-center justify-content-between accordion-item-row p-2 pb-1 pt-1 ${
        selected ? "selected" : null
      }`}
    >
      <span className="accordion-header-title">
        <FontAwesomeIcon size="xs" icon={isPrivate ? "lock" : "hashtag"} />
        &nbsp;&nbsp;&nbsp;{title}
      </span>
    </div>
  );
};

export default AccordionChannelItem;
