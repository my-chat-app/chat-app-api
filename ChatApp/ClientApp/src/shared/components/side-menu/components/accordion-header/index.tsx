import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import "./styles.scss";

export const AccordionHeader = ({ title }) => {
  return (
    <div
      id="accordion-header"
      className="d-flex flex-row align-items-center justify-content-between accordion-header-row p-2 pb-2 pt-2 pl-3 pr-3"
    >
      <span className="d-flex flex-row align-items-center justify-content-center caret-right-icon-container">
        <FontAwesomeIcon
          className="text caret-right-icon "
          size="xs"
          icon="caret-right"
        />
      </span>
      <span className="accordion-header-title">{title}</span>
      <span className="d-flex flex-row align-items-center justify-content-center add-icon-container">
        <FontAwesomeIcon className="text" size="1x" icon="plus" />
      </span>
    </div>
  );
};

export default AccordionHeader;
