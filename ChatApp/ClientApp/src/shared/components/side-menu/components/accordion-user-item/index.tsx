import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Image } from "react-bootstrap";
import "./styles.scss";

export const AccordionUserItem = ({
  id,
  name,
  presence = "away",
  onClick,
  selected = false,
  type,
  photoUrl
}) => {
  return (
    <div
      onClick={() => onClick(id,type)}
      id="accordion-user-item"
      className={`d-flex flex-row align-items-center justify-content-between accordion-item-row p-2 pb-1 pt-1 ${
        selected ? "selected" : null
      }`}
    >
      <span className="accordion-header">
        <div className="position-relative avatar-image-container d-inline-block">
          <Image
            className="avatar-image-container"
            rounded
            src={photoUrl}
          />
          <div
            className={`online-status-box ${
              presence === "online" ? "online" : "away"
            }`}
          ></div>
        </div>
        &nbsp;&nbsp;&nbsp;
        <span className="accordion-header-title">{name}</span>
        <span className="d-flex flex-row align-items-center justify-content-center close-icon-container">
          <FontAwesomeIcon className="text" size="1x" icon="close" />
        </span>
      </span>
    </div>
  );
};

export default AccordionUserItem;
