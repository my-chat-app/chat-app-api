import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import "./styles.scss";

export const WorkSpace = ({ name }) => {
  return (
    <div
      id="work-space-component"
      className="d-flex p-2 pb-2 pr-0 pt-2 pl-0 w-100 align-items-center justify-content-start"
    >
      <div className="d-flex flex-row w-100 align-items-center">
        <span className="text work-space-name m-0">{name}</span>
        &nbsp;&nbsp;&nbsp;
        <FontAwesomeIcon className="text-white" size="xs" icon="chevron-down" />
        &nbsp;&nbsp;
      </div>
    </div>
  );
};

export default WorkSpace;
