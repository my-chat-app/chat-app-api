import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import { useNavigate } from "react-router";
import useCurrentWorkSpace from "src/utils/hooks/useCurrentWorkSpace";

import AccordionChannelItem from "./components/accordion-channel-item";
import AccordionHeader from "./components/accordion-header";
import AccordionUserItem from "./components/accordion-user-item";
import WorkSpace from "./components/work-space";
import "./styles.scss";

export const SideMenu = ({ width, workSpace, channels = [], users = [] }) => {
  const [selectedAccordionListItem, setSelectedAccordionListItem] =
    useState(null);

  const navigate = useNavigate();
  const currentWorkSpace = useCurrentWorkSpace();


  const onClickUser = (id, type) => {
    setSelectedAccordionListItem(`${type}_${id}`);
    navigate(`/workSpace/${currentWorkSpace?.id}/user/${id}`);
  };

  const onClickChannel = (id, type) => {
    setSelectedAccordionListItem(`${type}_${id}`);
    navigate(`/workSpace/${currentWorkSpace?.id}/channel/${id}`);
  };

  return (
    <div
      id="side-menu"
      className="side-menu"
      style={{
        width: width + "vw",
      }}
    >
      <WorkSpace name={workSpace?.name} />
      <div id="side-drawer" className="pb-5 pr-0 menu overflow-auto">
        <Fragment>
          <AccordionHeader title={"channels"} />
          {channels.map((channel, channelIndex) => {
            return (
              <AccordionChannelItem
                id={channel?.id}
                key={channelIndex}
                title={channel?.name}
                isPrivate={channel?.isPrivate}
                onClick={onClickChannel}
                selected={
                  `${channel?.type}_${channel?.id}` ===
                  selectedAccordionListItem
                }
                type={channel?.type}
              />
            );
          })}

          <AccordionHeader title={"Direct Messages"} />
          {users.map((user, userIndex) => {
            return (
              <AccordionUserItem
                id={user?.id}
                key={userIndex}
                name={user?.name}
                presence={user?.presence}
                onClick={onClickUser}
                selected={
                  `${user?.type}_${user?.id}` === selectedAccordionListItem
                }
                type={user?.type}
                photoUrl={user?.photoUrl}
              />
            );
          })}
        </Fragment>
      </div>
    </div>
  );
};

const mapStateToProps = ({}: any) => ({});

const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
