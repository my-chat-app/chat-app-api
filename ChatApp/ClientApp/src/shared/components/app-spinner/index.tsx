import React from "react";
import { Spinner } from "react-bootstrap";
import "./styles.scss";

export const AppSpinner = ({ loading = false }) => {
  return loading ? (
    <div className="spinner-container">
      <Spinner animation="border" />
    </div>
  ) : null;
};

export default AppSpinner;
