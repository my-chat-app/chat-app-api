
import React from 'react';
import { Navigate } from 'react-router';
import {
  BrowserRouter as Router,
  Route as DefaultRoute,
} from 'react-router-dom';
import { DefaultLayout } from '../default-layout';

export const PrivateRoute = ({ component: Component, isAuthenticated, layout: Layout = DefaultLayout, ...rest }:any) => {
  return (
    <DefaultRoute
      {...rest}
      render={(props:any) => isAuthenticated ?
        <Component {...props} />
        : <Navigate to={{ pathname: '/login', 
        // state: { from: props.location }
       }} />}
    />
  )
}