import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Navigate, Outlet, useNavigate } from "react-router";
import {
  BrowserRouter as Router,
  Route as DefaultRoute,
} from "react-router-dom";
import "./styles.scss";

export const DefaultLayout = () => {
  const navigate = useNavigate();

  useEffect(() => {
    //@ts-ignore
    if (window.location.pathname === "/") {
      navigate("/login");
    }
    //@ts-ignore
  }, [window.location.pathname]);

  return (
    <div>
      <Outlet />
    </div>
  );
};

const mapStateToProps = ({}: any) => ({});

const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DefaultLayout);
