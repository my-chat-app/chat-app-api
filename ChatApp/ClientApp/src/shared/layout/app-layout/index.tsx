import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Image } from "react-bootstrap";
import { connect, useDispatch, useSelector } from "react-redux";
import { Outlet, useNavigate } from "react-router";
import { Col, Container } from "reactstrap";
import AppSpinner from "src/shared/components/app-spinner";
import Header from "src/shared/components/header";
import SideMenu from "src/shared/components/side-menu";
import useAllChannels from "src/utils/api-hooks/useAllChannels";
import useAllUsers from "src/utils/api-hooks/useAllUsers";
import useAllWorkSpaces from "src/utils/api-hooks/useAllWorkSpaces";
import "./styles.scss";

const MIN_DRAWER_WIDTH = 10;
const MAX_DRAWER_WIDTH = 30;

export const AppLayout = () => {
  const navigate = useNavigate();

  const { data: workSpaces, loading:workSpacesLoading } = useAllWorkSpaces();
  const { data: channels, loading:channelsLoading } = useAllChannels();
  const { data: users, loading:usersLoading } = useAllUsers();

  const [yValue, setYValue] = useState(15);
  const [draggable, setDraggable] = useState(true);
  useEffect(() => {
    //@ts-ignore
    if (window.location.pathname === "/") {
      navigate("/client");
    }
    //@ts-ignore
  }, [window.location.pathname]);

  const onResizeDrawer = (dragEvent) => {
    dragEvent.preventDefault();
    dragEvent.stopPropagation();
    //@ts-ignore
    var dragPercentage =
      //@ts-ignore
      (dragEvent.nativeEvent.clientX / window.innerWidth) * 100;
    if (dragPercentage === 0) {
      return;
    } else if (
      dragPercentage < MAX_DRAWER_WIDTH &&
      dragPercentage > MIN_DRAWER_WIDTH
    ) {
      setYValue(dragPercentage);
    }
  };

  const renderResizer = () => {
    return (
      <div
        draggable={draggable}
        onDrag={onResizeDrawer}
        className="resizer"
        style={{
          left: yValue + "vw",
        }}
      />
    );
  };

  const renderMainContent = () => {
    return (
      <div
        id="main-content"
        className="pb-5 overflow-scroll"
        style={{
          width: 100 - yValue + "vw",
          height: "100vh",
        }}
      >
        <Outlet />
      </div>
    );
  };

  return (
    <Container className="p-0" fluid>
      <Header />
      <Col xs={12} sm={12} lg={12} xxl={12}>
        <div className="d-flex flex-row position-relative">
          <SideMenu
            workSpace={workSpaces ? workSpaces[0] : {}}
            width={yValue}
            channels={channels}
            users={users}
          />
          {renderResizer()}
          {renderMainContent()}
        </div>
      </Col>

      {/* <Col className="pt-5" xs={12} sm={12} lg={12} xxl={12}>
        <Row>
          <Col className="side-menu position-fixed h-100" style={{cursor:'col-resize'}} xs={2} sm={2} lg={2} xxl={2}>
            d
          </Col>
          <Col style={{ width: "5px",cursor:'col-resize' }}>hi</Col>
          <Col className="side-menu no-float" xs={2} sm={2} lg={2} xxl={2}>
            d
          </Col>
        </Row>
      </Col> */}
      <AppSpinner loading={workSpacesLoading || channelsLoading || usersLoading} />
    </Container>
  );
};

export default AppLayout;
