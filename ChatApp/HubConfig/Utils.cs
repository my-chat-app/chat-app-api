﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.HubConfig
{
    public static class Utils
    {
        //public static Dictionary<string, string> UserIdWithConnectionId = new Dictionary<string, string>();

        public static readonly List<Tuple<long, string>> UserIdWithConnectionId = new List<Tuple<long, string>>();

        public static ConcurrentDictionary<string, bool> liveUsers = new ConcurrentDictionary<string, bool>();

        public static Dictionary<string, int> Levels = new Dictionary<string, int>()
        {
            {"Error",1 },
            {"Info",2 },
            {"Trace",3 }
        };

        public static string ToBase64(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.Unicode.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
