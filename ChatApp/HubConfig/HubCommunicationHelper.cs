﻿using ChatApp.HubConfig;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChatApp.Constants;

namespace ChatApp.HubConfig
{
    public class HubCommunicationHelper
    {
        IHubContext<ClientHub> _clientHubContext;

        public HubCommunicationHelper(IHubContext<ClientHub> clientHubContext)
        {
            _clientHubContext = clientHubContext;
        }

        #region SignalRMethods

        public string Notify(HubTypeEnum hubType, SignalRNotifyType type, object data, int? identification = null)
        {
            string message = string.Empty;
            switch (hubType)
            {
                case HubTypeEnum.OneClient:
                    message = NotifyOneClient(type, data, identification);
                    break;
                case HubTypeEnum.AllClient:
                    message = NotifyAllClient(type, data);
                    break;
                default:
                    message = "Wrong HubTypeEnum";
                    break;
            }
            return message;
        }

        private string NotifyOneClient(SignalRNotifyType type, object data, int? identityId)
        {

            if (Utils.UserIdWithConnectionId.FindIndex(x => x.Item1 == identityId) != -1)
            {
                foreach (var user in Utils.UserIdWithConnectionId.FindAll(x => x.Item1 == identityId))
                {
                    string connectionId = user.Item2;
                    _clientHubContext.Clients.Client(connectionId).SendAsync(type.ToString(), data);
                }
                return "Notify Sent";

            }
            return "UserIdWithConnectionId don't have this identityId";
        }

        private string NotifyAllClient(SignalRNotifyType type, object data)
        {
            _clientHubContext.Clients.All.SendAsync(type.ToString(), data);
            return "Notify Sended";
        }

        #endregion SignalRMethods
    }
}
