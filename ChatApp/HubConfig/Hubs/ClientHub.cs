﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using static ChatApp.Constants;

namespace ChatApp.HubConfig
{
    public class ClientHub : Hub
    {
        private string connectionId;
        private string token;

        //private JwtSecurityToken JwtSecurityToken;

        //public void InitializeConnection()
        //{
        //    this.connectionId = Context.ConnectionId;

        //}

        private string GetToken()
        {
            var httpContext = Context.GetHttpContext();
            token = httpContext.Request.Query["token"];
            return token;
        }

        public bool IsTokenExpired()
        {
            JwtSecurityToken jwtSecurityToken = new JwtSecurityTokenHandler().ReadJwtToken(GetToken());
            return jwtSecurityToken.ValidFrom > DateTime.UtcNow || DateTime.UtcNow > jwtSecurityToken.ValidTo;
        }

        public void NotifyClient(SignalRNotifyType type, string data, string connectionId = "")
        {
            string connId = connectionId == "" ? Context.ConnectionId : connectionId;

            if (IsTokenExpired())
            {
                Clients.Client(connId).SendAsync("Notify", SignalRNotifyType.Login, "Unauthorized");
            }
            else
            {
                Clients.Client(connId).SendAsync("Notify", type, data);
            }
        }

        public string GetConnectionId() => Context.ConnectionId;
    }
}
