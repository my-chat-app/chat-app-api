﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.Models
{
    public class AuthenticateRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public string HubConnectionId { get; set; }
    }

    public class AuthenticateRequestValidator : AbstractValidator<AuthenticateRequest>
    {
        public AuthenticateRequestValidator()
        {
            RuleFor(vm => vm.Username).NotEmpty().WithMessage("Username cannot be empty.");
            RuleFor(vm => vm.Password).NotEmpty().WithMessage("Password cannot be empty.");
            RuleFor(vm => vm.Password).Length(6, 15).WithMessage("Password must be between 6 and 15 characters.");
        }
    }
}
