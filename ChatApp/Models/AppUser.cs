﻿using ChatApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.Models
{
    public class AppUser
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Name { get; set; }


        public AppUser(AppUser appUser)
        {
            Id = appUser.Id;
            RoleId = appUser.RoleId;
            Name = appUser.Name;
        }
    }
}
