﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.Models.DTOs
{
    public static class enums
    {
        public enum LDA
        {
            [Description("Device Block 1")]
            Block1 = 1,

            [Description("Device Block 2")]
            Block2 = 2,

            [Description("Device Block 3")]
            Block3 = 3,

            [Description("Device Block 4")]
            Block4 = 4
        }

        /// <summary>
        /// Strike types
        /// </summary>
        public enum StrikeType
        {
            [Description("Exit Push Button")]
            EPB,

            [Description("Door Contact")]
            DoorContact,

            [Description("Relay")]
            Relay,
        }


        /// <summary>
        /// For building up the LPA (last parameter)
        /// </summary>
        public enum InputInstance
        {
            [Description("Device Tamper")]
            DeviceTamper = 0,

            [Description("Device Power Fail")]
            DevicePowerFail = 1,

            [Description("Door Contact")]
            DoorContact = 2,

            [Description("Exit Push Button")]
            EPB = 3,

            [Description("AUX 1")]
            AUX1 = 4,

            [Description("AUX 2")]
            AUX2 = 5
        }

        /// <summary>
        /// Threshold default Ids
        /// https://dev.azure-access.com/adme/asp-sdk/wikis/sdk/v1.13/core/Threshold
        /// TODO: In newer version of the SDK, may need modifications
        /// </summary>
        public enum ThresholdDefaultIds
        {
            [Description("Unsupervised normally closed")]
            Unsupervised_NC = 0,

            [Description("Unsupervised normally opened")]
            Unsupervised_NO = 1,

            [Description("Supervised 300/10K ohm normally closed")]
            Supervised_300_10K_NC = 2,

            [Description("Supervised 300/10K ohm normally opened")]
            Supervised_300_10K_NO = 3,

            [Description("Supervised 3K/4.5K ohm normally closed")]
            Supervised_3K_4_5K_NC = 4,

            [Description("Supervised 3K/4.5K ohm normally opened")]
            Supervised_3K_4_5K_NO = 5,

            [Description("Tamper unsupervised normally closed.")]
            Unsupervised_TNC = 6,

            [Description("Tamper unsupervised normally opened")]
            Unsupervised_TNO = 7,

            [Description("Tamper supervised 300/10K ohm normally closed")]
            Supervised_300_10K_TNC = 8,

            [Description("Tamper supervised 300/10K ohm normally opened")]
            Supervised_300_10K_TNO = 9,

            [Description("Tamper supervised 3K/4.5K ohm normally closed")]
            Supervised_3K_4_5K_TNC = 10,

            [Description("Tamper supervised 3K/4.5K ohm normally opened")]
            Supervised_3K_4_5K_TNO = 11,

            [Description("First Custom Id")]
            FirstCustomId = 100
        }

        /// <summary>
        /// For building up the LPA (last parameter)
        /// </summary>
        public enum RelayInstance
        {
            [Description("Relay 0")]
            Relay0 = 0,
            [Description("Relay 1")]
            Relay1 = 1,
        }

        /// <summary>
        /// Direction for the Reader
        /// </summary>
        public enum ReaderDirection
        {
            [Description("From Area A to B")]
            AreaAToB = 0,

            [Description("From Area B to A")]
            AreaBToA = 1
        }

        /// <summary>
        /// Time interval type
        /// </summary>
        public enum TimeIntervalType
        {
            [Description("Never")]
            Never = 1,

            [Description("Always")]
            Always = 2,

            [Description("SpecifiedTime")]
            SpecifiedTime = 3,
        }

        public enum PanelEvent { Denied = 0, Granted, NotRegistered }

        public enum WeekDaysDay { Sunday = 0, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday }

    }
}
