﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ChatApp.Models.DTOs
{
    public class ResponseDTO
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
