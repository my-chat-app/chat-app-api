﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatApp.Entities;

namespace ChatApp.Models.DTOs
{
    public class WorkSpacesDTO
    {
        public long? UserId { get; set; }
        public List<WorkSpace> WorkSpaces { get; set; }

    }
}
