﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.Models.Filters
{
    public class PagedTransactionFilter : SearchCriteriaBase
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ReaderId { get; set; }
        public int LocationId { get; set; }
        public int DoorId { get; set; }
    }
}
