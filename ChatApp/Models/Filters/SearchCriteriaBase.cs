﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.Models.Filters
{
    public class SearchCriteriaBase
    {
        public int page { get; set; }
        public int itemsPerPage { get; set; }
        public string sortBy { get; set; }
        public bool reverse { get; set; }
    }
}
