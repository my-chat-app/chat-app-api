﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp
{
    public static class Constants
    {
        public enum SignalRNotifyType
        {
            NewTransaction = 0,
            UpdatePanelStatus = 1,
            Login = 3
        }

        public enum HubTypeEnum { OneClient = 0, AllClient }

        public enum SetTimeType
        {
            NONE = 0,
            HOST_TIME = 1,
            SNTP_TIME = 2
        };

        public enum PanelType
        {
            ASP4 = 0,
            IC2 = 1
        };
    }
}
