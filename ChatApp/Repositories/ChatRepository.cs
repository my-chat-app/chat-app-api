﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ChatApp.Entities;
using ChatApp.Helpers;
using ChatApp.Models;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ChatApp.Models.DTOs;
using ChatApp.Interfaces.IRepositories;
using Microsoft.AspNetCore.Http;
using BC = BCrypt.Net.BCrypt;

namespace ChatApp.IRepositories
{
    public class ChatRepository : IChatRepository
    {
        private readonly AcDbContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ChatRepository(AcDbContext dbContext, IOptions<AppSettings> appSettings, IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _appSettings = appSettings.Value;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<IEnumerable<ChatMessageDTO>> GetChannelMessages(long channelId)
        {
            var currentUser = (User)_httpContextAccessor.HttpContext.Items["User"];
            var channelMessages = await _dbContext.ChatMessage.Where(x => x.ChannelId == channelId).ToListAsync();
            List<ChatMessageDTO> mappedChannelMessages = _mapper.Map<List<ChatMessage>, List<ChatMessageDTO>>(channelMessages);
            mappedChannelMessages.ForEach(x => x.isPersonal = currentUser != null ? currentUser.Id == x.SenderUserId : false);
            return mappedChannelMessages;

        }

        public async Task<IEnumerable<ChatMessageDTO>> GetUserMessages(long userId)
        {
            var currentUser = (User)_httpContextAccessor.HttpContext.Items["User"];
            var channelMessages = await _dbContext.ChatMessage.Where(x => (x.SenderUserId == userId || x.ReceiverUserId == userId) && x.ChannelId == null).ToListAsync();
            List<ChatMessageDTO> mappedChannelMessages = _mapper.Map<List<ChatMessage>, List<ChatMessageDTO>>(channelMessages);
            mappedChannelMessages.ForEach(x => x.isPersonal = currentUser != null ? currentUser.Id == x.SenderUserId : false);
            return mappedChannelMessages;
        }
    }

}

