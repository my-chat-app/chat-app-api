﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ChatApp.Entities;
using ChatApp.Helpers;
using ChatApp.Models;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ChatApp.Models.DTOs;
using ChatApp.Interfaces.IRepositories;
using Microsoft.AspNetCore.Http;
using BC = BCrypt.Net.BCrypt;

namespace ChatApp.Repositories
{

    public class UserRepository : IUserRepository
    {
        private readonly AcDbContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserRepository(AcDbContext dbContext, IOptions<AppSettings> appSettings, IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _appSettings = appSettings.Value;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest model)
        {

            var user = await _dbContext.Users.Where(x => x.Username == model.Username).FirstOrDefaultAsync();
            // return null if user not found
            if (user == null)
            {
                return null;
            }
            else if (!BC.Verify(model.Password, user.Password))
            {
                return null;
            }

            var role = await _dbContext.UserRoles.Where(x => x.UserId == user.Id).Select(x => x.Role).FirstOrDefaultAsync();

            if (role == null)
                return null;

            var token = generateJwtToken(user, role);

            AuthenticateResponse AuthenticateResponse = new AuthenticateResponse(user, token, role.Name);

            return AuthenticateResponse;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            List<UserDTO> getLocations = _mapper.Map<List<User>, List<UserDTO>>(await _dbContext.Users.ToListAsync());
            return await _dbContext.Users.ToListAsync();
        }
        public async Task<IEnumerable<UserRole>> GetAllData()
        {
            var currentUser = (User)_httpContextAccessor.HttpContext.Items["User"];
            return await _dbContext.UserRoles.Include(x => x.User).Include(x => x.Role).ToListAsync();
        }

        public async Task<User> GetById(long id)
        {
            var data = await _dbContext.UserRoles.Where(x => x.Id == id).Include(x => x.User).Include(x => x.Role).FirstOrDefaultAsync();
            if (data != null)
            {
                return data.User;
            }
            return null;
        }
        public async Task<string> GetUserRoleByUserId(long id)
        {
            var existingRoleId = await _dbContext.UserRoles.Where(x => x.Id == id).Select(x => x.RoleId).FirstOrDefaultAsync();
            var role = await _dbContext.Roles.Where(x => x.Id == existingRoleId).FirstOrDefaultAsync();
            if (role != null)
            {
                return role.Name;
            }
            return "";
        }
        public async Task<Role> GetUserRole(long id)
        {
            var existingRoleId = await _dbContext.UserRoles.Where(x => x.Id == id).Select(x => x.RoleId).FirstOrDefaultAsync();
            var role = await _dbContext.Roles.Where(x => x.Id == existingRoleId).FirstOrDefaultAsync();
            if (role != null)
            {
                return role;
            }
            return null;
        }

        public async Task<ResponseDTO> CreateUser(UserDTO userDTO)
        {
            User user = _mapper.Map<UserDTO, User>(userDTO);
            try
            {
                using (var dataBasetransaction = _unitOfWork.BeginTransaction())
                {
                    var createdUser = _dbContext.Users.Add(user);
                    if (_dbContext.Users.Any(x => x.Username == userDTO.Username))
                    {
                        dataBasetransaction.Rollback();
                        return new ResponseDTO
                        {
                            Message = "Username Already exist",
                            StatusCode = 401
                        };
                    }
                    await _unitOfWork.SaveChangesAsync();
                    var createdId = createdUser.Entity.Id;
                    _dbContext.UserRoles.Add(new UserRole
                    {
                        RoleId = (long)userDTO.RoleId,
                        UserId = createdId,
                    });
                    await _unitOfWork.SaveChangesAsync();
                    dataBasetransaction.Commit();
                    return new ResponseDTO
                    {
                        Message = "User creation success",
                        StatusCode = 201
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseDTO { Message = "User creation failed", StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        // helper methods
        private string generateJwtToken(User user, Role role)
        {
            // generate token that is valid for 7 days
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("id", user.Id.ToString()),
                    new Claim("role", role.Name),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<ResponseDTO> DeleteUser(long id)
        {
            using (var dataBasetransaction = _unitOfWork.BeginTransaction())
            {
                try
                {
                    UserRole user = new UserRole() { Id = id };
                    _dbContext.UserRoles.Attach(user);
                    _dbContext.UserRoles.Remove(user);
                    await _unitOfWork.SaveChangesAsync();
                    dataBasetransaction.Commit();
                    return new ResponseDTO
                    {
                        Message = "User deleted",
                        StatusCode = 201
                    };
                }
                catch (Exception ex)
                {
                    dataBasetransaction.Rollback();
                    return new ResponseDTO
                    {
                        Message = "Failed to delete User",
                        StatusCode = 400
                    };
                }
            }
        }

        public async Task<ResponseDTO> UpdateUser(UserDTO userDTO, long id)
        {

            UserRole existingUser = await _dbContext.UserRoles.Where(x => x.Id == id).FirstOrDefaultAsync();

            if (existingUser != null)
            {
                var user = await _dbContext.Users.Where(x => x.Username == userDTO.Username).FirstOrDefaultAsync();
                // return null if user not found
                if (user == null)
                {
                    return null;
                }
                else if (!BC.Verify(userDTO.Password, existingUser.User.Password))
                {
                    return new ResponseDTO
                    {
                        Message = "invalid Password",
                        StatusCode = 401
                    };
                }
                using (var dataBasetransaction = _unitOfWork.BeginTransaction())
                {
                    try
                    {
                        existingUser.User.Name = userDTO.Name;
                        existingUser.RoleId = userDTO.RoleId;
                        var updatedUser = _dbContext.UserRoles.Update(existingUser);
                        await _unitOfWork.SaveChangesAsync();
                        dataBasetransaction.Commit();
                        return new ResponseDTO
                        {
                            Message = "User updated",
                            StatusCode = 201
                        };
                    }
                    catch (Exception ex)
                    {
                        dataBasetransaction.Rollback();
                        return new ResponseDTO
                        {
                            Message = "Failed to update User",
                            StatusCode = 400
                        };
                    }
                }
            }
            return new ResponseDTO
            {
                Message = "Failed to update User",
                StatusCode = 400
            };
        }

        public async Task<IEnumerable<Role>> GetAllRole()
        {
            return await _dbContext.Roles.ToListAsync();
        }
    }
}

