﻿using ChatApp.Entities;
using ChatApp.Interfaces.IRepositories;
using System.Threading.Tasks;

namespace ChatApp.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AcDbContext _dbContext;

        public UnitOfWork(AcDbContext dbContext)
        {
            _dbContext = dbContext;
        }        

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public IDatabaseTransaction BeginTransaction()
        {
            return new EntityDatabaseTransaction(_dbContext);
        }
    }
}
