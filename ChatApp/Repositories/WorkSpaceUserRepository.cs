﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ChatApp.Entities;
using ChatApp.Helpers;
using ChatApp.Models;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ChatApp.Models.DTOs;
using ChatApp.Interfaces.IRepositories;
using Microsoft.AspNetCore.Http;
using BC = BCrypt.Net.BCrypt;

namespace ChatApp.Repositories
{

    public class WorkSpaceUserRepository : IWorkSpaceUserRepository
    {
        private readonly AcDbContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public WorkSpaceUserRepository(AcDbContext dbContext, IOptions<AppSettings> appSettings, IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _appSettings = appSettings.Value;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }



        public async Task<IEnumerable<User>> GetAllByWorkSpaceId(long workSpaceId)
        {
            var users = await _dbContext.WorkSpaceUsers.Where(x => x.WorkSpaceId == workSpaceId && x.IsActive == true && x.IsDeleted == false).Include(x => x.User).Select(x => x.User).ToListAsync();
            return users;
        }

    }
}

