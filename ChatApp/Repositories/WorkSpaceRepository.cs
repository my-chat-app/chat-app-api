﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ChatApp.Entities;
using ChatApp.Helpers;
using ChatApp.Models;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ChatApp.Models.DTOs;
using ChatApp.Interfaces.IRepositories;
using Microsoft.AspNetCore.Http;
using BC = BCrypt.Net.BCrypt;

namespace ChatApp.Repositories
{

    public class WorkSpaceRepository : IWorkSpaceRepository
    {
        private readonly AcDbContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public WorkSpaceRepository(AcDbContext dbContext, IOptions<AppSettings> appSettings, IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _appSettings = appSettings.Value;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }



        public async Task<WorkSpacesDTO> GetAllWorkspaces()
        {
            var currentUser = (User)_httpContextAccessor.HttpContext.Items["User"];
            if (currentUser != null)
            {
                var workSpaceUsers = _dbContext.WorkSpaceUsers.Where(x => x.UserId == currentUser.Id && x.IsActive == true && x.IsDeleted == false).Include(x => x.WorkSpace).AsEnumerable();
                var results = workSpaceUsers.GroupBy(p => p.UserId, p => p.WorkSpace,
                (key, g) =>
                new WorkSpacesDTO
                {
                    UserId = key,
                    WorkSpaces = g.ToList()
                }).FirstOrDefault();

                return results;
            }
            return null;
        }

    }
}

