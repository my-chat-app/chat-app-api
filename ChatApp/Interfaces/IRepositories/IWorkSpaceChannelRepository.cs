﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ChatApp.Entities;
using ChatApp.Helpers;
using ChatApp.Models;
using System.Threading.Tasks;
using ChatApp.Models.DTOs;

namespace ChatApp.IRepositories
{
    public interface IWorkSpaceChannelRepository
    {
        Task<IEnumerable<Channel>> GetAllByWorkSpaceId(long workSpaceId);

    }

}

