﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ChatApp.Entities;
using ChatApp.Helpers;
using ChatApp.Models;
using System.Threading.Tasks;
using ChatApp.Models.DTOs;

namespace ChatApp.IRepositories
{
    public interface IUserRepository
    {
        Task<AuthenticateResponse> Authenticate(AuthenticateRequest model);
        Task<IEnumerable<User>> GetAll();
        Task<IEnumerable<UserRole>> GetAllData();
        Task<User> GetById(long id);
        Task<string> GetUserRoleByUserId(long userId);
        Task<Role> GetUserRole(long userId);
        Task<ResponseDTO> CreateUser(UserDTO userDTO);
        Task<ResponseDTO> DeleteUser(long id);
        Task<ResponseDTO> UpdateUser(UserDTO userDTO, long id);
        Task<IEnumerable<Role>> GetAllRole();

    }

}

