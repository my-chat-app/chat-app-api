﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class WorkSpaceUser : BaseEntity
    {
        public long? WorkSpaceId { get; set; }
        public virtual WorkSpace WorkSpace { get; set; }


        public long? UserId { get; set; }
        public virtual User User { get; set; }


        public long? UserRoleId { get; set; }
        public virtual UserRole UserRole { get; set; }


        public string Name { get; set; }
        public string LogoUrl { get; set; }
    }
}
