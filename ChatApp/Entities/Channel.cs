﻿using ChatApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class Channel : BaseEntity
    {
        public string Name { get; set; }
        public string Topic { get; set; }
        public string Description { get; set; }
        public long? WorkSpaceId { get; set; }
        public bool IsPrivate { get; set; }
        public virtual WorkSpace WorkSpace { get; set; }
        public virtual ICollection<ChatMessage> ChatMessages { get; set; }

    }
}
