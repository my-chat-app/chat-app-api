﻿using ChatApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class ChannelUser : BaseEntity
    {
        public long? ChannelId { get; set; }
        public virtual Channel Channel { get; set; }

        public long? UserId { get; set; }
        public virtual User User { get; set; }
    }
}