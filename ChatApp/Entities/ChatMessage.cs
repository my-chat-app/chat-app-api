﻿using ChatApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class ChatMessage : BaseEntity
    {
        public string ContentType { get; set; }
        public string Content { get; set; }
        public long? SenderUserId { get; set; }
        public virtual User SenderUser { get; set; }
        public long? ReceiverUserId { get; set; }
        public virtual User ReceiverUser { get; set; }
        public long? ReplyMessageId { get; set; }
        public virtual ChatMessage ReplyMessage { get; set; }
        public long? ChannelId  { get; set; }
        public virtual Channel Channel { get; set; }
        public DateTime? MessageDate { get; set; }
    }
}

