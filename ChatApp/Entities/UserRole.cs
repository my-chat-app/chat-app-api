﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class UserRole : BaseEntity
    {
        public long? RoleId { get; set; }
        public long? UserId { get; set; }
        public virtual Role Role { get; set; }    
        public virtual User User { get; set; }
    }
}
