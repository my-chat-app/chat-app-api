﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class WorkSpace : BaseEntity
    {
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        // public virtual ICollection<WorkSpaceUser> WorkSpaceUsers { get; set; }
        // public virtual ICollection<Channel> Channels { get; set; }
    }
}
