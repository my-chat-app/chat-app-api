﻿using ChatApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }

        // public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
