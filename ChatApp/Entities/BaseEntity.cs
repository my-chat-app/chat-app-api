﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public long Id { get; set; }
        [JsonIgnore]
        public long? CreatedBy { get; set; }
        [JsonIgnore]
        public long? UpdatedBy { get; set; }
        [JsonIgnore]
        public long? DeletedBy { get; set; }
        [JsonIgnore]
        public DateTime? CreatedAt { get; set; } = DateTime.Now;
        [JsonIgnore]
        public DateTime? UpdatedAt { get; set; } = DateTime.Now;
        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }
        [JsonIgnore]
        public bool? IsDeleted { get; set; } = false;
        [JsonIgnore]
        public bool? IsActive { get; set; } = true;
    }
}
