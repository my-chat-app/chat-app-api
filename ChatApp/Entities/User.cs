﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class User: BaseEntity
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string PhotoUrl { get; set; }
        public string Email { get; set; }
        public string OnlineStatus { get; set; }
        public string WorkStatus { get; set; }
        public string Phone { get; set; }
        public string Timezone { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        // public virtual ICollection<WorkSpaceUser> WorkSpaceUsers { get; set; }
        // public virtual ICollection<ChatMessage> ChatMessages { get; set; }



    }

    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(vm => vm.Password).NotEmpty().WithMessage("Password cannot be empty.");
            RuleFor(vm => vm.Password).Length(6, 15).WithMessage("Password must be between 6 and 15 characters.");
        }
    }

}
