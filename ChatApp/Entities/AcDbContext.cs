﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.Entities
{
    public class AcDbContext : DbContext
    {
        public AcDbContext(DbContextOptions<AcDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSnakeCaseNamingConvention();

            if (!optionsBuilder.IsConfigured)
            {
                var builder = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                var configuration = builder.Build();
                string connectionString = $"{configuration["ConnectionStrings:DefaultConnection"]}";

                optionsBuilder.UseLazyLoadingProxies().UseNpgsql(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<User>()
            //    .HasKey(ent => new { ent.Id });

            //modelBuilder.Entity<Role>()
            //    .HasKey(ent => new { ent.Id });

            //modelBuilder.Entity<WorkSpace>()
            //    .HasKey(ent => new { ent.Id });

            //modelBuilder.Entity<WorkSpaceUser>()
            //    .HasKey(ent => new { ent.Id });


            modelBuilder.Entity<ChatMessage>()
                .HasOne(bc => bc.SenderUser)
                .WithMany(/*b => b.WorkSpaceUsers*/)
                .HasForeignKey(bc => bc.SenderUserId);
            
            modelBuilder.Entity<ChatMessage>()
                .HasOne(bc => bc.ReceiverUser)
                .WithMany(/*b => b.WorkSpaceUsers*/)
                .HasForeignKey(bc => bc.ReceiverUserId);

            modelBuilder.Entity<ChatMessage>()
                .HasOne(x => x.ReplyMessage)
                .WithOne()
                .HasForeignKey(typeof(ChatMessage), "ReplyMessageId");
                
                // modelBuilder.Entity<ChatMessage>().HasOne(b => b.parent).WithMany(a => a.children).HasForeignKey(b => b.ParentID);

            // modelBuilder.Entity<Role>().Ignore(x => x.UserRoles);

            //modelBuilder.Entity<WorkSpaceUser>()
            //    .HasOne(x => x.User)
            //    .WithMany()
            //    .HasForeignKey(x => x.UserId);

            //modelBuilder.Entity<WorkSpaceUser>()
            //    .HasOne(x => x.UserRole)
            //    .WithMany()
            //    .HasForeignKey(x => x.UserRoleId);

            //modelBuilder.Entity<UserRole>()
            //    .HasOne(x => x.Role)
            //    .WithMany()
            //    .HasForeignKey(x => x.RoleId);

            //modelBuilder.Entity<UserRole>()
            //    .HasOne(x => x.User)
            //    .WithMany()
            //    .HasForeignKey(x => x.UserId);

        }


        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<WorkSpace> WorkSpaces { get; set; }
        public DbSet<WorkSpaceUser> WorkSpaceUsers { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Channel> Channels { get; set; }
        public DbSet<ChannelUser> ChannelUsers { get; set; }
        public DbSet<ChatMessage> ChatMessage { get; set; }

       
    }
}
