﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ChatApp.Migrations
{
    public partial class ABC_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_channel_work_spaces_work_space_id",
                table: "channel");

            migrationBuilder.DropPrimaryKey(
                name: "pk_channel",
                table: "channel");

            migrationBuilder.RenameTable(
                name: "channel",
                newName: "channels");

            migrationBuilder.RenameIndex(
                name: "ix_channel_work_space_id",
                table: "channels",
                newName: "ix_channels_work_space_id");

            migrationBuilder.AddPrimaryKey(
                name: "pk_channels",
                table: "channels",
                column: "id");

            migrationBuilder.CreateTable(
                name: "channel_users",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    channel_id = table.Column<int>(type: "integer", nullable: false),
                    user_id = table.Column<int>(type: "integer", nullable: false),
                    created_by = table.Column<long>(type: "bigint", nullable: true),
                    updated_by = table.Column<long>(type: "bigint", nullable: true),
                    deleted_by = table.Column<long>(type: "bigint", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_channel_users", x => x.id);
                    table.ForeignKey(
                        name: "fk_channel_users_channels_channel_id",
                        column: x => x.channel_id,
                        principalTable: "channels",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_channel_users_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "chat_message",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    content_type = table.Column<string>(type: "text", nullable: true),
                    content = table.Column<string>(type: "text", nullable: true),
                    sender_user_id = table.Column<int>(type: "integer", nullable: false),
                    receiver_user_id = table.Column<int>(type: "integer", nullable: false),
                    reply_message_id = table.Column<int>(type: "integer", nullable: true),
                    message_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<long>(type: "bigint", nullable: true),
                    updated_by = table.Column<long>(type: "bigint", nullable: true),
                    deleted_by = table.Column<long>(type: "bigint", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_chat_message", x => x.id);
                    table.ForeignKey(
                        name: "fk_chat_message_chat_message_reply_message_id",
                        column: x => x.reply_message_id,
                        principalTable: "chat_message",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_chat_message_users_receiver_user_id",
                        column: x => x.receiver_user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_chat_message_users_sender_user_id",
                        column: x => x.sender_user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_channel_users_channel_id",
                table: "channel_users",
                column: "channel_id");

            migrationBuilder.CreateIndex(
                name: "ix_channel_users_user_id",
                table: "channel_users",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ix_chat_message_receiver_user_id",
                table: "chat_message",
                column: "receiver_user_id");

            migrationBuilder.CreateIndex(
                name: "ix_chat_message_reply_message_id",
                table: "chat_message",
                column: "reply_message_id");

            migrationBuilder.CreateIndex(
                name: "ix_chat_message_sender_user_id",
                table: "chat_message",
                column: "sender_user_id");

            migrationBuilder.AddForeignKey(
                name: "fk_channels_work_spaces_work_space_id",
                table: "channels",
                column: "work_space_id",
                principalTable: "work_spaces",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_channels_work_spaces_work_space_id",
                table: "channels");

            migrationBuilder.DropTable(
                name: "channel_users");

            migrationBuilder.DropTable(
                name: "chat_message");

            migrationBuilder.DropPrimaryKey(
                name: "pk_channels",
                table: "channels");

            migrationBuilder.RenameTable(
                name: "channels",
                newName: "channel");

            migrationBuilder.RenameIndex(
                name: "ix_channels_work_space_id",
                table: "channel",
                newName: "ix_channel_work_space_id");

            migrationBuilder.AddPrimaryKey(
                name: "pk_channel",
                table: "channel",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "fk_channel_work_spaces_work_space_id",
                table: "channel",
                column: "work_space_id",
                principalTable: "work_spaces",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
