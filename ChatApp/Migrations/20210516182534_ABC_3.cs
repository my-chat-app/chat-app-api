﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatApp.Migrations
{
    public partial class ABC_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "channel_id",
                table: "chat_message",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_chat_message_channel_id",
                table: "chat_message",
                column: "channel_id");

            migrationBuilder.AddForeignKey(
                name: "fk_chat_message_channels_channel_id",
                table: "chat_message",
                column: "channel_id",
                principalTable: "channels",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_chat_message_channels_channel_id",
                table: "chat_message");

            migrationBuilder.DropIndex(
                name: "ix_chat_message_channel_id",
                table: "chat_message");

            migrationBuilder.DropColumn(
                name: "channel_id",
                table: "chat_message");
        }
    }
}
