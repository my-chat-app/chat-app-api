﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatApp.Migrations
{
    public partial class mig_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_chat_message_users_user_id",
                table: "chat_message");

            migrationBuilder.DropIndex(
                name: "ix_chat_message_reply_message_id",
                table: "chat_message");

            migrationBuilder.DropIndex(
                name: "ix_chat_message_user_id",
                table: "chat_message");

            migrationBuilder.DropColumn(
                name: "user_id",
                table: "chat_message");

            migrationBuilder.CreateIndex(
                name: "ix_chat_message_reply_message_id",
                table: "chat_message",
                column: "reply_message_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_chat_message_reply_message_id",
                table: "chat_message");

            migrationBuilder.AddColumn<long>(
                name: "user_id",
                table: "chat_message",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_chat_message_reply_message_id",
                table: "chat_message",
                column: "reply_message_id");

            migrationBuilder.CreateIndex(
                name: "ix_chat_message_user_id",
                table: "chat_message",
                column: "user_id");

            migrationBuilder.AddForeignKey(
                name: "fk_chat_message_users_user_id",
                table: "chat_message",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
