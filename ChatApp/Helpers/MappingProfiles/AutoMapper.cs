﻿using ChatApp.Entities;
using ChatApp.Models.DTOs;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BC = BCrypt.Net.BCrypt;

namespace ASPNetCoreCodeBase.Security.MappingProfiles
{
    public class DtoAutoMapper : Profile
    {
        public DtoAutoMapper()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>().ForMember(
                d => d.Password, opt => opt.MapFrom(x => BC.HashPassword(x.Password))
                );

            CreateMap<ChatMessage, ChatMessageDTO>();
        }

    }
}




