﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatApp.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using ChatApp.IRepositories;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
public class AuthorizeAttribute : Attribute, IAsyncAuthorizationFilter
{
    public string Roles = "";
    public AuthorizeAttribute(params string[] roles) : base()
    {
        Roles = string.Join(",", roles);
    }

    public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
    {
        try
        {
            var contextItems =  context.HttpContext.Items;
            if (contextItems.Count > 1)
            {
                var user =  (User)context.HttpContext.Items["User"];
                if (user == null)
                {
                    // not logged in
                    context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
                }
                else
                {
                    var userService = context.HttpContext.RequestServices.GetService(typeof(IUserRepository)) as IUserRepository;
                    var currentRole = await userService.GetUserRoleByUserId(user.Id);
                    if (!Roles.Contains(currentRole))
                    {
                        context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
                    }
                }
            }
            else
            {
                context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
        }
        
    }

}
