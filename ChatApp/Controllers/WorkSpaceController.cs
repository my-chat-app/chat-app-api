﻿using Microsoft.AspNetCore.Mvc;
using ChatApp.Models;
using ChatApp.Helpers;
using Microsoft.AspNetCore.Authorization;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using ChatApp.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using ChatApp.HubConfig;
using System;
using static ChatApp.Constants;
using Newtonsoft.Json;

namespace ChatApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WorkSpaceController : ControllerBase
    {
        private IWorkSpaceRepository _workSpaceRepository;
        private IHubContext<ClientHub> _clientHub;
        private readonly HubCommunicationHelper _hubCommunicationHelper;

        public WorkSpaceController(IWorkSpaceRepository workSpaceRepository,
            IHubContext<ClientHub> clientHub, HubCommunicationHelper hubCommunicationHelper)
        {
            _workSpaceRepository = workSpaceRepository;
            _clientHub = clientHub;
            _hubCommunicationHelper = hubCommunicationHelper;
        }


        // [Authorize(Roles.User)]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var workSpaceList = await _workSpaceRepository.GetAllWorkspaces();
            if (workSpaceList != null)
            {
                return Ok(workSpaceList);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}