﻿using Microsoft.AspNetCore.Mvc;
using ChatApp.Models;
using ChatApp.Helpers;
using Microsoft.AspNetCore.Authorization;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using ChatApp.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using ChatApp.HubConfig;
using System;
using static ChatApp.Constants;
using Newtonsoft.Json;

namespace ChatApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WorkSpaceChannelsController : ControllerBase
    {
        private IWorkSpaceChannelRepository _workSpaceChannelRepository;
        private IHubContext<ClientHub> _clientHub;
        private readonly HubCommunicationHelper _hubCommunicationHelper;

        public WorkSpaceChannelsController(IWorkSpaceChannelRepository workSpaceChannelRepository,
            IHubContext<ClientHub> clientHub, HubCommunicationHelper hubCommunicationHelper)
        {
            _workSpaceChannelRepository = workSpaceChannelRepository;
            _clientHub = clientHub;
            _hubCommunicationHelper = hubCommunicationHelper;
        }


        // [Authorize(Roles.User)]
        [HttpGet("workSpace/{workSpaceId}/channels")]
        public async Task<IActionResult> GetAllByWorkSpaceId(long workSpaceId)
        {
            var channels = await _workSpaceChannelRepository.GetAllByWorkSpaceId(workSpaceId);
            if (channels != null)
            {
                return Ok(channels);
            }
            else
            {
                return NotFound();
            }
        }
    }
}