﻿using Microsoft.AspNetCore.Mvc;
using ChatApp.Models;
using ChatApp.Helpers;
using Microsoft.AspNetCore.Authorization;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using ChatApp.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using ChatApp.HubConfig;
using System;
using static ChatApp.Constants;
using Newtonsoft.Json;

namespace ChatApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserRepository _userRepository;
        private IHubContext<ClientHub> _clientHub;
        private readonly HubCommunicationHelper _hubCommunicationHelper;

        public UsersController(IUserRepository userRepository,
            IHubContext<ClientHub> clientHub, HubCommunicationHelper hubCommunicationHelper)
        {
            _userRepository = userRepository;
            _clientHub = clientHub;
            _hubCommunicationHelper = hubCommunicationHelper;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(AuthenticateRequest model)
        {
            var response = await _userRepository.Authenticate(model);
            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            if (Utils.UserIdWithConnectionId.FindIndex(x => x.Item1 == response.Id && x.Item2 == model.HubConnectionId) == -1)
            {
                Utils.UserIdWithConnectionId.Add(new Tuple<long, string>(response.Id, model.HubConnectionId));
            }
            return Ok(response);
        }

        // [Authorize(Roles.User)]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userRepository.GetAllData();
            return Ok(users);
        }

        //[Authorize(Roles.Admin)]
        [HttpPost]
        public async Task<IActionResult> CreateUser(UserDTO user)
        {
            var response = await _userRepository.CreateUser(user);
            if (response.StatusCode == StatusCodes.Status201Created)
            {
                return Ok(response);
            }
            else if (response.StatusCode == StatusCodes.Status401Unauthorized)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        //[Authorize(Roles.Admin)]
        [HttpGet("role/{Id}")]
        public async Task<IActionResult> GetRole(int id)
        {
            var users = await _userRepository.GetUserRole(id);
            return Ok(users);
        }

        //[Authorize(Roles.Admin)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            var response = await _userRepository.GetById(id);
            return Ok(response);
        }

        //[Authorize(Roles.Admin)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(UserDTO userDTO, int id)
        {
            var response = await _userRepository.UpdateUser(userDTO, id);
            if (response.StatusCode == StatusCodes.Status201Created)
            {
                return Ok(response);
            }
            else if (response.StatusCode == StatusCodes.Status401Unauthorized)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }
        // [Authorize(Roles.Admin)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var response = await _userRepository.DeleteUser(id);
            return Ok(response);
        }

        // [Authorize(Roles.Admin)]
        [HttpGet("Role")]
        public async Task<IActionResult> GetAllRoles()
        {
            var roles = await _userRepository.GetAllRole();
            return Ok(roles);
        }
    }
}