﻿using Microsoft.AspNetCore.Mvc;
using ChatApp.Models;
using ChatApp.Helpers;
using Microsoft.AspNetCore.Authorization;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using ChatApp.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using ChatApp.HubConfig;
using System;
using static ChatApp.Constants;
using Newtonsoft.Json;

namespace ChatApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WorkSpaceUsersController : ControllerBase
    {
        private IWorkSpaceUserRepository _workSpaceUserRepository;
        private IHubContext<ClientHub> _clientHub;
        private readonly HubCommunicationHelper _hubCommunicationHelper;

        public WorkSpaceUsersController(IWorkSpaceUserRepository workSpaceUserRepository,
            IHubContext<ClientHub> clientHub, HubCommunicationHelper hubCommunicationHelper)
        {
            _workSpaceUserRepository = workSpaceUserRepository;
            _clientHub = clientHub;
            _hubCommunicationHelper = hubCommunicationHelper;
        }


        // [Authorize(Roles.User)]
        [HttpGet("workSpace/{workSpaceId}/users")]
        public async Task<IActionResult> GetAllByWorkSpaceId(long workSpaceId)
        {
            var users = await _workSpaceUserRepository.GetAllByWorkSpaceId(workSpaceId);
            if (users != null)
            {
                return Ok(users);
            }
            else
            {
                return NotFound();
            }
        }
    }
}