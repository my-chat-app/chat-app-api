﻿using Microsoft.AspNetCore.Mvc;
using ChatApp.Models;
using ChatApp.Helpers;
using Microsoft.AspNetCore.Authorization;
using ChatApp.IRepositories;
using System.Threading.Tasks;
using ChatApp.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using ChatApp.HubConfig;
using System;
using static ChatApp.Constants;
using Newtonsoft.Json;

namespace ChatApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChatController : ControllerBase
    {
        private IChatRepository _chartRepository;
        private IHubContext<ClientHub> _clientHub;
        private readonly HubCommunicationHelper _hubCommunicationHelper;

        public ChatController(IChatRepository chartRepository,
            IHubContext<ClientHub> clientHub, HubCommunicationHelper hubCommunicationHelper)
        {
            _chartRepository = chartRepository;
            _clientHub = clientHub;
            _hubCommunicationHelper = hubCommunicationHelper;
        }


        // [Authorize(Roles.User)]
        [HttpGet("channels/{channelId}/messages")]
        public async Task<IActionResult> GetAllMessagesByChannelId(long channelId)
        {
            var messages = await _chartRepository.GetChannelMessages(channelId);
            if (messages != null)
            {
                return Ok(messages);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("users/{userId}/messages")]
        public async Task<IActionResult> GetAllMessagesByUserId(long userId)
        {
            var messages = await _chartRepository.GetUserMessages(userId);
            if (messages != null)
            {
                return Ok(messages);
            }
            else
            {
                return NotFound();
            }
        }
    }
}